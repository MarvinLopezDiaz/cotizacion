function Limpiar(){
    document.getElementById('txtValor').value = ('');
    document.getElementById('txtEnganche').value = ('');
    document.getElementById('txtFinarziar').value = ('');
    document.getElementById('txtPago').value = ('');
}

function CalcularTF(){
    let valor = document.getElementById('txtValor').value;
    let meses = document.getElementById('planes').value;
    let en = document.getElementById('txtEnganche');
    let finanzas = document.getElementById('txtFinarziar');
    let pago = document.getElementById('txtPago');
    let porcentaje = 0;
    switch (meses){
        case '12':
            porcentaje = 0.125
            break;
        case '18':
            porcentaje = 0.172
            break;
        case '24':
            porcentaje = 0.21
            break;
        case '36':
            porcentaje = 0.26
            break;
        case '48':
            porcentaje = 0.45
            break;
        default:
          console.log('Error en el sistema');
    }
    const enga = (valor * 0.3).toFixed(2);  
    const fin = (valor - enga + porcentaje).toFixed(2);
    const paM = (fin / meses).toFixed(2);
    finanzas.value = fin;
    en.value = enga;
    pago.value = paM;
}








